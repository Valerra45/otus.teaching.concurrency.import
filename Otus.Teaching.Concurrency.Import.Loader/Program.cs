﻿using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using System;
using System.Diagnostics;
using System.IO;


namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.csv");
        private static int _dataCount = 47;

        static void Main(string[] args)
        {
            if (args != null && args.Length == 2)
            {
                _dataFilePath = args[1];

                var stsrtInfo = new ProcessStartInfo()
                {
                    ArgumentList = { _dataFilePath, _dataCount.ToString() },
                    FileName = args[0]
                };

                var process = Process.Start(stsrtInfo);

                Console.WriteLine($"Loader started with process Id {process.Id}...");

                process.WaitForExit();
            }
            else
            {
                if (args != null && args.Length == 1)
                {
                    _dataFilePath = args[0];
                }

                Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

                GenerateCustomersDataFile();
            }

            var parser = new CsvParser(_dataFilePath);

            using var ctx = new ApplicationDbContext();

            ctx.Database.EnsureDeleted();
            ctx.Database.EnsureCreated();

            var loader = new FakeDataLoader(parser);

            loader.LoadData();
        }

        static void GenerateCustomersDataFile()
        {
            Console.WriteLine("Generating csv data...");

            var csvGenerator = new CsvGenerator(_dataFilePath, _dataCount);
            csvGenerator.Generate();

            Console.WriteLine($"Generated csv data in {_dataFilePath}...");
            Console.WriteLine();
        }
    }
}
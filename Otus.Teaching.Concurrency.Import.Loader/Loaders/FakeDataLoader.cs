using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class FakeDataLoader
        : IDataLoader
    {
        private const int TreadCount = 8;
        private readonly IDataParser<List<Customer>> _parser;

        public FakeDataLoader(IDataParser<List<Customer>> parser)
        {
            _parser = parser;
        }

        public void LoadData()
        {
            var waitHandles = new WaitHandle[TreadCount];

            var sw = new Stopwatch();

            Console.WriteLine("Loading data...");

            sw.Start();

            var customers = _parser.Parse();

            var sizePart = customers.Count / TreadCount;

            for (int i = 0; i < TreadCount; i++)
            {
                var customersQuery = customers.Skip(sizePart * i);

                var customersPart = i < TreadCount - 1
                    ? customersQuery.Take(sizePart).ToList()
                    : customersQuery.ToList();

                var are = new AutoResetEvent(false);

                ThreadPool.QueueUserWorkItem(x => LoadTread(customersPart, are));

                waitHandles[i] = are;
            }

            WaitHandle.WaitAll(waitHandles);

            sw.Stop();

            Console.WriteLine("Loaded data... ");
            Console.WriteLine($"Loading time: {sw.Elapsed.TotalSeconds:F}");
        }

        private void LoadTread(List<Customer> customers, AutoResetEvent waitHandle)
        {
            using var ctx = new ApplicationDbContext();

            var repository = new CustomerRepository(ctx);

            customers.ForEach(x => repository.AddCustomer(x));

            repository.SaveChanges();

            waitHandle.Set();
        }
    }
}
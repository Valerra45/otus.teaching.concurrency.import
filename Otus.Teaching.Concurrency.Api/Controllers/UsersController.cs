﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Repositories;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController
        : ControllerBase
    {
        private readonly IUserRepository _userRepository;

        public UsersController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<User>>> GetAllUsersAsync()
        {
            var users = await _userRepository.GetAll();

            return Ok(users);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<User>> GetUserAsync(int id)
        {
            var user = await _userRepository.GetByIdAsync(id);

            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        [HttpPost]
        public async Task<ActionResult> AddUserAsync([FromBody] User user)
        {
            if (await _userRepository.Exist(user.Id))
            {
                return new StatusCodeResult(409);
            }

            await _userRepository.AddAsync(user);
            await _userRepository.SaveChangesAsync();

            return Ok();
        }
    }
}

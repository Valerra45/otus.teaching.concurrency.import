﻿using Otus.Teaching.Concurrency.Import.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Core.Repositories
{
    public interface IUserRepository
    {
        Task<User> GetByIdAsync(int id);

        Task<IEnumerable<User>> GetAll();

        Task AddAsync(User user);

        Task<int> SaveChangesAsync();

        Task<bool> Exist(int id);
    }
}

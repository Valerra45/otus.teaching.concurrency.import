﻿using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser : BaseParser
    {
        public XmlParser(string path) : base(path)
        {

        }

        public override List<Customer> Parse()
        {
            using var stream = File.OpenRead(_path);

            return ((CustomersList)new XmlSerializer(typeof(CustomersList), new XmlRootAttribute("Customers"))
                .Deserialize(stream))
                .Customers;
        }
    }
}
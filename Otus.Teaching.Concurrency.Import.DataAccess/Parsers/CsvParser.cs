﻿using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using ServiceStack.Text;
using System.Collections.Generic;
using System.IO;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class CsvParser : BaseParser
    {
        public CsvParser(string path) : base(path)
        {

        }

        public override List<Customer> Parse()
        {
            using var stream = File.OpenText(_path);

            return CsvSerializer.DeserializeFromReader<CustomersList>(stream).Customers;
        }
    }
}

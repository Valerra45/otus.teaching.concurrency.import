﻿using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Generic;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public abstract class BaseParser
        : IDataParser<List<Customer>>
    {
        protected readonly string _path;

        public BaseParser(string path)
        {
            _path = path;
        }

        public abstract List<Customer> Parse();

    }
}

﻿namespace Otus.Teaching.Concurrency.Import.DataAccess.Data
{
    public class DbInitializer : IDbInitializer
    {
        private readonly ApplicationDbContext _ctx;

        public DbInitializer(ApplicationDbContext ctx)
        {
            _ctx = ctx;
        }

        public void InitializeDb()
        {
            _ctx.Database.EnsureCreated();
        }
    }
}

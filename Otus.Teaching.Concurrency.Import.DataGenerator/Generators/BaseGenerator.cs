﻿using Otus.Teaching.Concurrency.Import.Handler.Data;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public abstract class BaseGenerator : IDataGenerator
    {
        protected readonly string _fileName;
        protected readonly int _dataCount;

        public BaseGenerator(string fileName, int dataCount)
        {
            _fileName = fileName;
            _dataCount = dataCount;
        }

        public abstract void Generate();
    }
}

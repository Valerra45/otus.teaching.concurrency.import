﻿using Bogus;
using Otus.Teaching.Concurrency.Import.Core.Entities;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class RandomUserGenerator
    {
        public static Faker<User> Generate(int id)
        {
            var userFaker = new Faker<User>()
                .CustomInstantiator(f => new User()
                {
                    Id = id++
                })
                .RuleFor(x => x.FirstName, y => y.Person.FirstName)
                .RuleFor(x => x.LastName, y => y.Person.LastName)
                .RuleFor(x => x.Email, y => y.Internet.Email())
                .RuleFor(x => x.Login, y => y.Person.UserName)
                .RuleFor(x => x.Password, y => y.Person.Phone);

            return userFaker;
        }
    }
}

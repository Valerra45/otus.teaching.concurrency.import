using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using System.IO;
using System.Xml.Serialization;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class XmlGenerator : BaseGenerator
    {
        public XmlGenerator(string fileName, int dataCount)
            : base(fileName, dataCount)
        {

        }

        public override void Generate()
        {
            var customers = RandomCustomerGenerator.Generate(_dataCount);
            using var stream = File.Create(_fileName);
            new XmlSerializer(typeof(CustomersList)).Serialize(stream, new CustomersList()
            {
                Customers = customers
            });
        }
    }
}
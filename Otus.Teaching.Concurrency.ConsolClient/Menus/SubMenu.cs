﻿using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.ConsoleClient.Menus
{
    public class SubMenu
    {
        private readonly HttpClient _httpClient;
        private int _id = 10;

        public SubMenu(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<HttpResponseMessage> GetAll()
        {
            return await _httpClient.GetAsync("users/");
        }

        public async Task<HttpResponseMessage> GetById(int id)
        {
            return await _httpClient.GetAsync($"users/{id}");
        }

        public async Task<HttpResponseMessage> Add()
        {
            var user = GenerateUser(_id++);

            return await _httpClient.PostAsync("users/", user, new JsonMediaTypeFormatter());
        }

        private User GenerateUser(int id)
        {
            return RandomUserGenerator.Generate(id);
        }
    }
}

﻿using Newtonsoft.Json;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.ConsoleClient.Menus
{
    public class MainMenu
    {
        private readonly SubMenu _subMenu;

        private string _json = default;

        private HttpResponseMessage _response;

        private static int _arrow = 0;

        private static string[] _menu =
        {
            "Get all",
            "Get by id",
            "Add faker user",
            "Exit"
        };

        public MainMenu(SubMenu subMenu)
        {
            _subMenu = subMenu;
        }

        public async Task Show()
        {
            while (true)
            {
                Console.Clear();

                if (_arrow < (int)Menu.Get)
                {
                    _arrow = (int)Menu.Exit;
                }
                else if (_arrow > (int)Menu.Exit)
                {
                    _arrow = (int)Menu.Get;
                }

                for (int i = 0; i < _menu.Length; i++)
                {
                    Console.WriteLine($"{(_arrow == i ? ">>" : "  ")} {_menu[i]}");
                }

                ConsoleKey key = Console.ReadKey().Key;

                if (key == ConsoleKey.UpArrow)
                {
                    _arrow--;
                }
                else if (key == ConsoleKey.DownArrow)
                {
                    _arrow++;
                }
                else if (key == ConsoleKey.Enter)
                {
                    switch ((Menu)_arrow)
                    {
                        case Menu.Get:
                            Console.Clear();

                            _response = await _subMenu.GetAll();

                            if (_response.StatusCode != HttpStatusCode.OK)
                            {
                                Console.WriteLine($"Get all result {((int)_response.StatusCode)}");
                            }
                            else
                            {
                                _json = await _response.Content.ReadAsStringAsync();

                                var users = JsonConvert.DeserializeObject<List<User>>(_json);

                                foreach (var user in users)
                                {
                                    Console.WriteLine($"{user.Id} {user.FirstName} {user.LastName} {user.Login} {user.Email}");
                                }
                            }

                            Console.WriteLine();
                            Console.WriteLine("Press any key to continue...");
                            Console.ReadKey();

                            break;
                        case Menu.GetById:
                            Console.Clear();

                            Console.Write("Enter user id: ");
                            var id = Console.ReadLine();

                            _response = await _subMenu.GetById(int.Parse(id));

                            if (_response.StatusCode != HttpStatusCode.OK)
                            {
                                Console.WriteLine($"Get all result {((int)_response.StatusCode)}\n");
                            }
                            else
                            {
                                _json = await _response.Content.ReadAsStringAsync();

                                var user = JsonConvert.DeserializeObject<User>(_json);

                                Console.WriteLine($"{user.Id} {user.FirstName} {user.LastName} {user.Login} {user.Email}\n");
                            }

                            Console.WriteLine("Press any key to continue...");
                            Console.ReadKey();

                            break;
                        case Menu.Add:
                            Console.Clear();

                            _response = await _subMenu.Add();

                            Console.WriteLine($"Added result {((int)_response.StatusCode)}");
                            Console.WriteLine("Press any key to continue...\n");
                            Console.ReadKey();

                            break;
                        case Menu.Exit:
                            Console.Clear();

                            return;
                    }
                }
            }
        }

        private enum Menu
        {
            Get,
            GetById,
            Add,
            Exit
        }
    }
}

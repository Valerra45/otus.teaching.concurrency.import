﻿using Otus.Teaching.Concurrency.ConsoleClient.Menus;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.ConsoleClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            using var httpClient = new HttpClient(new HttpClientHandler());

            httpClient.BaseAddress = new Uri("https://localhost:44378/");

            await new MainMenu(new SubMenu(httpClient)).Show();
        }
    }
}
